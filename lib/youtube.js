var con = require('./connection');
var econ = require('./econsole');

class Youtube {
	constructor() {
		this.emptyVdata = {
			snippet: { title: '-' },
			statistics: {
				viewCount: '-',
				likeCount: '-',
				dislikeCount: '-',
				favoriteCount: '-',
				commentCount: '-'
			}
		};
		this.errorVdata = {
			snippet: { title: 'Fehler' },
			statistics: {
				viewCount: '-',
				likeCount: '-',
				dislikeCount: '-',
				favoriteCount: '-',
				commentCount: '-'
			}
		};
		this.mongofilter = {
			key: 1,
			snippet: { title: 1, description: 1 },
			statistics: {
				viewCount: 1,
				likeCount: 1,
				dislikeCount: 1,
				favoriteCount: 1,
				commentCount: 1
			}
		};
	}

	getEntriesServerless() {
		return this.getEntriesL;
	}
	getEntries(cb) {
		return new Promise((resolve, reject) => {
			con.OnConnected.add((mongo) => {
				var result = mongo.db('eapi').collection('youtube').estimatedDocumentCount();
				result.then((ires) => {
					this.getEntriesL = ires;
					resolve(ires);
				});
			});
		});
	}
	getDatalessEntriesServerless() {
		return this.getDatalessEntriesL;
	}
	getDatalessEntries(cb) {
		return new Promise((resolve, reject) => {
			con.OnConnected.add((mongo) => {
				var result = mongo.db('eapi').collection('youtube').countDocuments({
					$or: [ { snippet: { $exists: false } }, { 'ytdata.error': { $exists: true } } ]
				});
				result.then((ires) => {
					this.getDatalessEntriesL = ires;
					resolve(ires);
				});
			});
		});
	}

	//Removes Random video Entries until max is reached
	// Per GB Server RAM about 10k entries are okay
	flushData(max, amount = 1) {
		return new Promise((resolve, reject) => {
			this.getEntries().then((videosindb) => {
				// 2000 - 209 => 1791
				var maxRemove = videosindb - max;
				if (maxRemove > amount)
					econ.log(
						'Youtube',
						'Should remove ' +
							maxRemove +
							' Videos but limited to ' +
							amount +
							' there are currently ' +
							videosindb +
							'. I will try to remove ' +
							amount +
							' Videos now...'
					);
				if (maxRemove == amount) econ.log('Youtube', 'Removing exactly ' + amount + ' Videos');
				if (maxRemove < amount) {
					econ.log(
						'Youtube',
						'Removing no Videos (' +
							videosindb +
							' Videos in DB | tolerance is ' +
							amount +
							' | max is ' +
							max +
							')'
					);
					resolve();
					return;
				}

				let promises = [];
				for (let i = 0; i < amount; i++) {
					promises.push(
						new Promise((jresolve, jreject) => {
							con.OnConnected.add((mongo) => {
								var result = mongo
									.db('eapi')
									.collection('youtube')
									.aggregate([ { $sample: { size: 3 } } ]);
								result.next().then(async (ires) => {
									mongo.db('eapi').collection('youtube').deleteOne({ key: ires.key });
									jresolve();
								});
							});
						})
					);
				}
				Promise.all(promises).then(() => {
					resolve();
					this.getEntries().then((result) => {
						econ.log('Youtube', 'Removing succesful: There are ' + result + ' Videos left');
					});
				});
			});
		});
	}

	removeLargeData() {
		return new Promise((resolve, reject) => {
			this.getEntries().then((videosindb) => {
				let promises = [];
				/*  */
				promises.push(
					new Promise((jresolve, jreject) => {
						con.OnConnected.add((mongo) => {
							var result = mongo.db('eapi').collection('youtube').find({});
							result.next().then(async (ires) => {
								Object.bsonsize(ires);

								//mongo.db('eapi').collection('youtube').deleteOne({ key: ires.key });
								jresolve();
							});
						});
					})
				);

				Promise.all(promises).then(() => {
					resolve();
					this.getEntries().then((result) => {
						econ.log('Youtube', 'Removing succesful: There are ' + result + ' Videos left');
					});
				});
			});
		});
	}

	getRandomVideo(query) {
		if (query == undefined) query = {};
		if (query.key != undefined) return this.getVideo(query.key);
		return new Promise((resolve, reject) => {
			con.OnConnected.add((mongo) => {
				var requestPipe = [];

				var match = {};
				match = { $match: { $or: [] } };
				var allowedLanguages = [ 'de', 'en', 'ignore' ];
				var allowedForceTitle = [ 'true', 'false', 'ignore' ];
				if (query.lang != undefined && allowedLanguages.some((e) => e == query.lang)) {
					if (query.lang != 'ignore') match.$match.$or.push({ 'snippet.defaultLanguage': query.lang });
				} else {
					if (query.lang != undefined) {
						reject(JSON.stringify({ code: 901, error: "Invalid Language '" + query.lang + "'" }));
						return;
					}
				}

				if (false)
					if (req.query.forceTitle != undefined && allowedForceTitle.some((e) => e == req.query.forceTitle)) {
						if (req.query.forceTitle == 'ignore' || req.query.forceTitle != 'false')
							match.$match.$or.push({ snippet: { $exists: false } });
					} else {
						res.send(JSON.stringify({ code: 902, error: 'Invalid Data State' }));
						return;
					}

				if (match.$match.$or.length > 0) requestPipe.push(match);

				var sampleSize = { $sample: { size: 1 } };
				requestPipe.push(sampleSize);
				requestPipe.push({ $project: this.mongofilter });

				//econ.log('Youtube', JSON.stringify(requestPipe));
				var result = mongo.db('eapi').collection('youtube').aggregate(requestPipe);

				result.next().then(async (ires) => {
					if (!!ires && !!ires.snippet && !!ires.snippet.description) {
						ires.snippet.description = nl2br(ires.snippet.description);
					}
					else {
						if (!!ires) {ires={};if (ires.snippet != undefined) {ires.snippet={};if (ires.snippet["description"] != undefined) {ires.snippet["description"]="";}}}
						
						
					}
					resolve(ires);
				});
			});
		});
	}
	getVideo(vidid) {
		return new Promise((resolve, reject) => {
			con.OnConnected.add((mongo) => {
				let result = mongo.db('eapi').collection('youtube').countDocuments({ key: vidid });
				//econ.log('Youtube', 'vidid');
				//econ.log('Youtube', vidid);
				//econ.log('Youtube', 'result');
				result.then((ires) => {
					if (ires == 0) reject();
					if (ires == 1) {
						let jresult = mongo.db('eapi').collection('youtube').find({ key: vidid });
						jresult.next().then(async (jres) => {
							if (!!jres && !!jres.snippet && !!jres.snippet.description) {
								jres.snippet.description = nl2br(jres.snippet.description);
							}
							else {
								if (!!jres) {jres={};}
								if (!!jres.snippet) {jres.snippet={};}
								if (!!jres.snippet.description) {jres.snippet.description="";}
							}
							resolve(jres);
						});
					}
				});

				/*  */
			});
		});
	}
	getAllVideos() {
		return new Promise((resolve, reject) => {
			con.OnConnected.add((mongo) => {
				let result = mongo.db('eapi').collection('youtube').countDocuments({ });
				result.then((ires) => {
					if (ires == 0) reject(0);
					if (ires >= 1) {
						var jresult = mongo.db('eapi').collection('youtube').find({ });
						resolve(jresult);
					}
				});

				/*  */
			});
		});
	}
}
module.exports = new Youtube();

function nl2br(str) {
	//console.log(typeof str);
	if (typeof str == 'string') return str.replace(/(?:\r\n|\r|\n)/g, '<br>') + 'MANIPULATED';
	else return 'No String'; 
}
