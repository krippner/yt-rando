#!/usr/bin/env node
const fs = require('fs');var global_memory = [];
const puppeteer = require('puppeteer');
var mobj;
class Crawler {
    async crawl(url="https://www.youtube.com/watch?v=FiVw6zjgw24"){
        return new Promise((resolve, reject)=>{
        (async () => {
            try{
                var i = 0;
                const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-gpu', '--disable-setuid-sandbox'],defaultViewport:{height:2080,width:1080}});
                const page = await browser.newPage();
                await page.goto(url, {waitUntil: 'networkidle2'});
                //await page.screenshot({path: 'example.png'});
                var content = await page.content();
                await browser.close();
            } catch(e){
                reject();
                return;
            }

            resolve(readLinks(content));
          })();
        });
    }
}

function readLinks(filedata){
    var memory = [];
    var reg = /(http[s]?:\/\/)(www\.)?(youtube\.com)(\/watch\?v=([a-zA-Z0-9_\-]*))?/gm;
    var reg_vids = /watch\?v=([a-zA-Z0-9_-]*)/gm;
    var reg_channel = /(http[s]?:\/\/)(www\.)?(youtube\.com)(\/watch\?v=([a-zA-Z0-9_\-]*))?/gm;

    filedata.toString().match(reg).map(v=>youtube(v,reg,5)).map((varr)=>{if(varr[0] != undefined){var v = varr[0];if(v.length==11 && !memory.some(iv => iv.key == v)){memory.push({valid:true,key:v,video:true});}if(v.length!=11 && !memory.some(iv => iv.key == v)){memory.push({valid:false,key:v});}}});
    filedata.toString().match(reg_vids).map(v=>youtube(v,reg_vids,1)).map((varr)=>{if(varr[0] != undefined){var v = varr[0];if(v.length==11 && !memory.some(iv => iv.key == v)){memory.push({valid:true,key:v,video:true});}if(v.length!=11 && !memory.some(iv => iv.key == v)){memory.push({valid:false,key:v});}}});
    filedata.toString().match(reg_channel).map(v=>youtube(v,reg_channel,5)).map((varr)=>{if(varr[0] != undefined){var v = varr[0];if(!memory.some(iv => iv.key == v)){memory.push({valid:true,key:v,channel:true});}}});

    function youtube(text, regex, group)
    {
        let output = [], match;while (match = regex.exec(text)) output.push(match[group]);return output;
    }
    return memory;
}
mobj=new Crawler();
module.exports=mobj;

function nl2br (str, is_xhtml) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Philip Peterson
    // +   improved by: Onno Marsman
    // +   improved by: Atli Þór
    // +   bugfixed by: Onno Marsman
    // +      input by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +   improved by: Maximusya
    // *     example 1: nl2br('Kevin\nvan\nZonneveld');
    // *     returns 1: 'Kevin<br />\nvan<br />\nZonneveld'
    // *     example 2: nl2br("\nOne\nTwo\n\nThree\n", false);
    // *     returns 2: '<br>\nOne<br>\nTwo<br>\n<br>\nThree<br>\n'
    // *     example 3: nl2br("\nOne\nTwo\n\nThree\n", true);
    // *     returns 3: '<br />\nOne<br />\nTwo<br />\n<br />\nThree<br />\n'
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display
  
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
  }