class mEvent {
	constructor({ name, singlefire }) {
		this.Name = name;
		this.SingleFire = singlefire;
		this.Fired = 0;
		this.Delegates = [];
	}

	execute(data) {
		this.Fired++;
		this.SingleData = data;
		this.Delegates.forEach((d) => {
			if ('function' == typeof d.fn) d.fn(data);
			if ('function' != typeof d) {
				return;
			}
			d(data.fn);
		});
	}

	// Function intervals until it's completed at
	// least one succesful conditional call
	//
	// howOften => how often the succesful call has to be made | -1 for forever
	//
	interval({ conditionFn, interval, data, howOften }) {
		if (howOften == undefined) howOften = -1;
		var executed = 0;
		var myinterval = setInterval(function() {
			if (conditionFn != undefined) {
				if (conditionFn()) {
					this.execute(data);
					executed++;

					if (!howOften == -1 || !howOften == executed) {
						clearInterval(myinterval);
					}
				}
			} else {
				this.execute(data);
			}
		}, interval);
	} // AndExecute

	// Function intervals until it's completed at
	// least one succesful conditional call
	// gives up after first call.
	timeout({ conditionFn, timeout, data }) {
		setTimeout(function() {
			if (conditionFn != undefined) {
				if (conditionFn()) this.execute(data);
			} else {
				this.execute(data);
			}
		}, timeout);
	} // AndExecute

	static oclock({oclock=0, minutes=0, seconds=0,reschedule=true,data,fn,debug=false}) {
        var now = new Date();

		var cfn = (!!fn?fn:this.execute);
		var cdata = (!!data?()=>data:()=>this.data);
		

		var until = new Date(now.getFullYear(), now.getMonth(), now.getDate(), oclock, minutes, seconds, 0) - now;
		if (until < 0) {
			until += 86400000; // it's after 10am, try 10am tomorrow.
		}
		setTimeout(function() {
			cfn(cdata());
			if(reschedule)
            setInterval(()=>{
                cfn(cdata());
            },86400000)
		}, (debug?0:until));
	}

	add(fn) {
		// Wenn dieses Event schon ausgeführt wurde,
		// wird bei einer Registrierung sofort die Funktion
		// ausgeführt mit den daten des ersten events!
		this.SingleData != undefined ? fn(this.SingleData) : null;

		// In jedem Fall wird die Funktion dem Stapel hinzugefügt
		this.Delegates.push({ fn, executedCnt: 0 });
	}
};

module.exports = mEvent;