class mDate {
  GermanTime(){
    let date = new Date();
    return date.getHours().toString().padStart(2,'0') + ":"+date.getMinutes().toString().padStart(2,'0') +":"+date.getSeconds().toString().padStart(2,'0') ;
  }
  GermanDate(){
    let date = new Date();
    return date.getDay().toString().padStart(2,'0') + "."+date.getMonth().toString().padStart(2,'0') +"."+date.getFullYear() ;
  }
}
module.exports = new mDate();