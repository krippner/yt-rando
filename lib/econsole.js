var path = require('path');

module.exports = {
	log(tag, msg) {
		if(tag==undefined) tag="NULL"
		if(!tag) var tag = __filename;
		console.log('['.padEnd(2) + tag.padEnd(16) + ' ] '.padStart(2) + JSON.stringify(msg));
	}
};
