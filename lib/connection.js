const MongoClient = require('mongodb').MongoClient;
var econ = require('./econsole');
var TAG = 'MongoDB';
var mEvent = require('./event');
const assert = require('assert');
const util = require('util');
const url = 'mongodb://127.0.0.1:27017';

const dbName = 'eapi';
var client = new MongoClient(url, { useNewUrlParser: true });
var savedClient;
var OnConnected = new mEvent({ name: 'OnConnected', singlefire: true });
var OnDisconnected = new mEvent({ name: 'OnDisconnected', singlefire: true });

var retry_count = 0;
function connect_r() {
  client = new MongoClient(url, { useNewUrlParser: true });
  client.connect(function(err, iclient) {
		if (err != null) {
      process.stdout.write('\033[0G'); 
      process.stdout.write("\r\033[0G  Could not connect.. Trying again in 3s [ Retry #"+retry_count+" ]\033[0G\r");
      retry_count++;
			setTimeout(connect_r,3000);
			return;
		}

		econ.log(TAG, 'Connected correctly to server');

		savedClient = iclient;
    OnConnected.execute(savedClient);
    OnDisconnected.add(()=>{
      process.exit(1);
    });
    iclient.on('close', ()=>{
      OnDisconnected.execute();
    })
	});
}
connect_r();

module.exports = {
	db() {
		return savedClient.db;
	},
	client() {
		return savedClient;
	},
  OnConnected: OnConnected,
  OnDisconnected: OnDisconnected
};

function sleep(ms) {
	var start = new Date().getTime();
	var expire = start + ms;
	while (new Date().getTime() < expire) {}
	return;
}
