var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var lessMiddleware = require('less-middleware');
var logger = require('morgan');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var youtubeRouter = require('./routes/youtube');
var giftRouter = require('./routes/gift');

var youtube = require('./lib/youtube');
var date = require('./lib/date');
var __counter = 0;




var app = express()
  

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

var calls=0;
app.use(logger(function (tokens, req, res) {
  return [
    date.GermanTime(),
    date.GermanDate(),
    ++calls,
    tokens.method(req, res),
    tokens.url(req, res),
    tokens.status(req, res),
    tokens.res(req, res, 'content-length'), '-',
    tokens['response-time'](req, res), 'ms'
  ].join(' ')
}));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

/* app.use (function (req, res, next) {
  if (req.secure) {
          // request was via https, so do no special handling
          next();
  } else {
          // request was via http, so redirect to https
          res.redirect('https://' + req.headers.host + req.url);
  }
}); */

app.use('/gift', giftRouter);
app.use('/IPShare', giftRouter);
app.use('/lottery', giftRouter);
app.use('/index', indexRouter);
app.use('/users', usersRouter);
app.use('/', youtubeRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;



