var express = require('express');
var ytcrawl = require('../lib/ytcrawl');
var con = require('../lib/connection');
var mEvent = require('../lib/event');
var YouTube = require('../lib/youtube');
var econ = require('../lib/econsole');
var TAG = 'Router::Youtube';
const fetch = require('node-fetch');
var router = express.Router();
var sm = require('sitemap');

var requestCodes = []; 

sitemap = sm.createSitemap({
	hostname: 'https://youtube.benjamin-krippner.de',
	cacheTime: 600000, // 600 sec - cache purge period
	urls: allURLs
});

var cachedPages = [];

var allURLs = [
];

YouTube.getAllVideos().then(async(cursor) => {
  econ.log('Sitemap', 'Start');
    while (await cursor.hasNext()) {
      // REFERENCE: http://youtube.benjamin-krippner.de/watch?w=oRUXeAiX6Zs
      const doc = await cursor.next();
			//console.count("YT")
      allURLs.push({ url: "/watch?w="+doc.key, changefreq: 'daily', priority: 0.8})
		};
	sitemap = sm.createSitemap({
		hostname: 'https://youtube.benjamin-krippner.de',
		cacheTime: 600000, // 600 sec - cache purge period
		urls: allURLs
	});
  econ.log('Sitemap', 'Ende');
});

router.get('/sitemap.xml', function(req, res) {
  sitemap.toXML( function (err, xml) {
      if (err) {
        return res.status(500).end();
      }
      res.header('Content-Type', 'application/xml');
      res.send( xml );
  });
});

router.get('/update', function(req, res, next) {
	process.exit(2);
}); 
router.get('/', function(req, res, next) {
	con.OnConnected.add(async (mongo) => {
		getRandomVideoJSONData(req.query).then(async (vdata) => {

			res.render('youtube/rando', {id:vdata.id,vdata:vdata});

		}).catch(()=>{
			var vdata = {};
			vdata.dataless = -1;
			vdata.videos = -1;

			res.render('youtube/rando', {
				title: 'Youtube Randomizer!',
				id: 'http://about:blank;',
				vdata: YouTube.emptyVdata
			});
		});
	});
});

router.get('/crawl', function(req, res, next) {
	var myrqcode = Math.floor(Math.random() * 10000);
	res.render('youtube/crawl_listing', { title: 'Express', rqcode: myrqcode });
	//res.send("<script src='./javascripts/ytcrawl.js'></script><b>Accepted Request...</b><br><ans rqcode='"+myrqcode+"'></ans>");
});

router.get('/watch', function(req, res, next) {
	//econ.log('Youtube',req.query);
	YouTube.getRandomVideo({ key: req.query.w })
		.then(async function(ires) {
			var vdata = {};

			//econ.log(TAG, ires);
			var vdata = YouTube.emptyVdata;

			!!ires.snippet ? (vdata.snippet = ires.snippet) : '';
			!!ires.statistics ? (vdata.statistics = ires.statistics) : '';

			vdata.dataless = await YouTube.getDatalessEntries();
			vdata.videos = await YouTube.getEntries();
			vdata.id = 'https://www.youtube.com/embed/' + req.query.w;
			vdata.key = req.query.w;

			res.render('youtube/rando', {
				title: 'Youtube Randomizer!',
				id: vdata.id,
				vdata: vdata,
				refresh: false
			});
		})
		.catch(() => {
			console.log("ERRORED in /watch")
			res.render('youtube/rando', {
				title: 'Youtube Randomizer!',
				id: 'https://www.youtube.com/embed/' + req.query.w,
				vdata: YouTube.errorVdata,
				refresh: false
			});
			return;
		});
});

router.get('/myfile.txt', function(req, res, next) {
	res.send('Hello');
});

function getRandomVideoJSONData(query) {
	return new Promise((resolve, reject) => {
		YouTube.getRandomVideo(query)
			.then(async function(ires) {
				var vdata = {};

				//econ.log(TAG, 'Called for new Video');
				//econ.log(TAG, ires);
				var vdata = YouTube.emptyVdata;

				!!ires.snippet ? (vdata.snippet = ires.snippet) : '';
				!!ires.statistics ? (vdata.statistics = ires.statistics) : '';

				vdata.dataless = 10200;
				vdata.videos = 10200;
				vdata.id = 'https://www.youtube.com/embed/' + ires.key;
				vdata.key = ires.key;
				

				resolve(vdata);
			})
			.catch(() => {
				reject(YouTube.errorVdata);
			});
	});
}

router.get('/getnew', function(req, res, next) {
	//econ.log('Youtube',req.query);
	getRandomVideoJSONData(req.query).then((vdata) => {
		res.send(JSON.stringify(vdata));
	});
});

router.get('/result/:id', function(req, res, next) {
	if (requestCodes[req.params.id] != undefined) {
		res.send(requestCodes[req.params.id]);
	} else {
		res.send('{}');
	}
});

var CrawlInterval;
con.OnConnected.add((mongo) => {
	//CrawlInterval = setInterval(crawl, 1000 * 60 * 5);
	//crawl();
	YouTube.flushData(10200,100);
});
con.OnDisconnected.add((mongo) => {
	clearInterval(CrawlInterval);
});

//setTimeout(crawl, 5000);
//var datacrawlInterval = setInterval(datacrawl, 300);

function datacrawl() {
	//
	//econ.log(TAG,"Startet Data Crawling Routine -+''+ ");
	getDatalessYtVideoEntry((videoentry) => {
		econ.log(TAG, 'Data Crawling... ' + videoentry.key);
		datacrawlAndPushIntoDB(videoentry);
		econ.log(TAG, 'Data Crawling... 2 ');
		YouTube.getDatalessEntries().then((data) => {
			econ.log(TAG, 'Dataless Videos: ' + data);
		});
	});
}
function crawl() {
	YouTube.flushData(10200,12);
	econ.log(TAG, "Startet Crawling Routine -+''+ ");
	getRandomYtVideoEntry((videoentry) => {
		econ.log(TAG, 'Crawling... ' + videoentry.key);
		crawlAndPushIntoDB(videoentry);
	});
	crawlMainSiteAndPushIntoDB();
}
function crawlAndPushIntoDB(key = 'FkkEjWhamTI') {
	ytcrawl.crawl('https://www.youtube.com/watch?v=' + key).then((values) => {
		//econ.log(TAG,'Mongo RET');
		con.OnConnected.add((mongo) => {
			//requestCodes[myrqcode] = values;
			values.forEach((el) => {
				mongo
					.db('eapi')
					.collection('youtube')
					.updateOne({ key: { $eq: el.key } }, { $set: el }, { upsert: true });
			});
			econ.log(TAG, 'Found ' + values.length + ' new Videos!');
		});
	}); //3.6.3
}
function datacrawlAndPushIntoDB(videoentry) {
	fetch(
		'https://www.googleapis.com/youtube/v3/videos?part=statistics,snippet&id=' +
			videoentry.key +
			'&key=AIzaSyAHjb4rlsKY1cssZm8BU6EzP_29xT6SJz0'
	)
		.then((res) => {
			if (res.ok) {
				return res.json();
			} else {
				throw 'API Limit reached!';
			}
		})
		.catch((err) => {
			econ.log(TAG, err);
			clearInterval(datacrawlInterval);
			econ.log(TAG, 'Removed Data Crawl');

			mEvent.oclock({
				oclock: 9,
				minutes: 2,
				data: {},
				fn: () => {
					setInterval(() => datacrawl(), 8640);
				},
				reschedule: false
			});

			throw 'Can not be resolved';
		})
		.then((json) => {
			videoentry.snippet = json.items[0].snippet;
			videoentry.statistics = json.items[0].statistics;
			con.OnConnected.add((mongo) => {
				mongo
					.db('eapi')
					.collection('youtube')
					.updateOne({ key: { $eq: videoentry.key } }, { $set: videoentry }, { upsert: true });
			});
		})
		.catch((err) => {
			econ.log(TAG, err);
		});
}

function crawlMainSiteAndPushIntoDB() {
	ytcrawl.crawl('https://www.youtube.com/').then((values) => {
		//econ.log(TAG,'Mongo RET');
		con.OnConnected.add((mongo) => {
			//requestCodes[myrqcode] = values;
			values.forEach((el) => {
				mongo
					.db('eapi')
					.collection('youtube')
					.updateOne({ key: { $eq: el.key } }, { $set: el }, { upsert: true });
			});
			econ.log(TAG, 'Found ' + values.length + ' new Videos!');
		});
	});
}

function getRandomYtVideoEntry(cb) {
	//econ.log(TAG,'getRandvidentry');
	con.OnConnected.add((mongo) => {
		//econ.log(TAG,'onconEv in getRandvidentr');
		var result = mongo.db('eapi').collection('youtube').aggregate([ { $sample: { size: 1 } } ]);
		result.next().then((ires) => {
			cb(ires);
		});
	});
}
function getDatalessYtVideoEntry(cb) {
	//econ.log(TAG,'getRandvidentry');
	con.OnConnected.add((mongo) => {
		//econ.log(TAG,'onconEv in getRandvidentr');
		var result = mongo
			.db('eapi')
			.collection('youtube')
			.aggregate([
				{ $match: { $or: [ { snippet: { $exists: false } }, { 'ytdata.error': { $exists: true } } ] } },
				{ $sample: { size: 1 } }
			]);
		result.next().then((ires) => {
			cb(ires);
		});
	});
}

//Conformity Updates
con.OnConnected.add((mongo) => {
	//db.getCollection('youtube').find({},{"ytdata.items":1})
	mongo.db('eapi').collection('youtube').find({}, { 'ytdata.items': 1 }).forEach((result) => {
		if (result.ytdata != undefined) {
			if (!!result.ytdata.error == false) {
				econ.log(TAG, result.ytdata);
				if (!!result.ytdata.items[0].snippet && !!result.ytdata.items[0].statistics) {
					mongo.db('eapi').collection('youtube').updateOne(
						{ key: { $eq: result.key } },
						{
							$set: {
								snippet: result.ytdata.items[0].snippet,
								statistics: result.ytdata.items[0].statistics
							},
							$unset: { ytdata: '' }
						}
					);
				}
			} else {
				mongo.db('eapi').collection('youtube').updateOne(
					{ key: { $eq: result.key } },
					{
						$unset: { ytdata: '' }
					}
				);
			}
		}
		if (!!result.statistics)
			if (typeof result.statistics.viewCount == typeof '') {
				mongo.db('eapi').collection('youtube').updateOne(
					{ key: { $eq: result.key } },
					{
						$set: {
							'statistics.viewCount': Number(result.statistics.viewCount)
						},
						$unset: { ytdata: '' }
					}
				);
			}
	});

	/* 	TOLLIST QUERY
mongo
		.db('eapi')
		.collection('youtube')
		.find({}, { 'statistics.viewCount': 1 })
		.limit(10)
		.sort({ 'statistics.viewCount': -1 })
		.forEach((result) => {
			econ.log(TAG,
				Number(
					(result.statistics.dislikeCount == 0 || result.statistics.dislikeCount == undefined
						? -1
						: result.statistics.dislikeCount) /
						Number(
							result.statistics.likeCount == 0 || result.statistics.likeCount == undefined
								? 1
								: result.statistics.likeCount
						) *
						100
				)
					.toString()
					.substring(0, 4) +
					' ' +
					pad('              ', result.statistics.viewCount.toString(), false) +
					' ' +
					result.snippet.title
			);
		}); */
});

YouTube.getEntries((data) => {
	//econ.log(TAG, 'Dataless Videos: ' + data);
});
YouTube.getDatalessEntries((data) => {
	//econ.log(TAG, 'Videos: ' + data);
});
module.exports = router;

function pad(pad, str, padLeft) {
	if (typeof str === 'undefined') return pad;
	if (padLeft) {
		return (pad + str).slice(-pad.length);
	} else {
		return (str + pad).substring(0, pad.length);
	}
}
